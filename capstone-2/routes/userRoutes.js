const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')

const auth = require('../auth')

// User Registration
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// Login user
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Toggle admin permission / Set user as Admin (Admin Only)
router.put("/toggle-admin", auth.verify, (request, response) => {

	let data = {
		userId: request.body.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.toggleAdmin(data).then((result) => {
		response.send(result);
	})
})

// Retrieve user details
router.get("/details", auth.verify, (request,response) => {

	let userId = auth.decode(request.headers.authorization).id;

	UserController.getUserDetails(userId).then((result) => {
		response.send(result)
	})
})

// Get ALL users (added feature)
router.get("/", auth.verify, (request,response) => {
	let data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.getAllUsers(data).then((result) => {
		response.send(result)
	})
})



module.exports = router