const express = require('express')
const router = express.Router()

const CartController = require('../controllers/CartController')
const auth = require('../auth')


// Added products
router.post("/", auth.verify, (request, response) => {
	let data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		userId: auth.decode(request.headers.authorization).id,
		product: request.body
	}

	CartController.addCartProduct(data).then((result) => {
		response.send(result)
	})
})

// Get SINGLE Cart
router.get("/getCartByUserId", auth.verify, (request, response) => {
	let data = {
		userId: auth.decode(request.headers.authorization).id
	}
	CartController.getCartByUserId(data).then((result) => {
		response.send(result)
	})
})

// Get SINGLE Cart
router.post("/getCartByUserIdAndProductId", auth.verify, (request, response) => {
	let data = {
		userId: auth.decode(request.headers.authorization).id,
		productId: request.body.productId
	}
	CartController.getCartByUserIdAndProductId(data).then((result) => {
		response.send(result)
	})
})


// Change product quantities from cart & subtotal for each item
router.put("/:id", auth.verify, (request, response) => {

	CartController.updateCartProduct(request.body).then((result) => {
		response.send(result)
	})
})

// Remove products from cart
router.delete("/:id", auth.verify, (request, response) => {
	CartController.deleteCartProduct(request.params.id).then((result) => {
		response.send(result)
	})
})

// Total price for all items (checkout)
router.post("/checkout", auth.verify, (request, response) => {
	CartController.checkout(request.body).then((result) => {
		response.send(result)
	})
})



module.exports = router