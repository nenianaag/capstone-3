const express = require('express')
const router = express.Router()

const OrderController = require('../controllers/OrderController')
const auth = require('../auth')

// Non-admin User Checkout (Create Order)
router.post("/create", auth.verify, (request, response) => {
	const data = {
		order: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		userId: auth.decode(request.headers.authorization).id
	}

	OrderController.createOrder(data).then((result) => {
		response.send(result)
	})
})

// Retrieve authenticated user's orders
router.get("/user-orders", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		userId: auth.decode(request.headers.authorization).id,
	}

	OrderController.getUserOrders(data).then((result) => {
		response.send(result)
	})
} )


// Retrieve all orders (Admin Only)
router.get("/", auth.verify, (request, response) => {
	let data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	OrderController.getAllOrders(data).then((result) => {
		response.send(result)
	})
})

module.exports = router


