// Dependencies
const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')

// Local Imports
const cartRoutes = require('./routes/cartRoutes')
const orderRoutes = require('./routes/orderRoutes')
const productRoutes = require('./routes/productRoutes')
const userRoutes = require('./routes/userRoutes')


// App configurations
dotenv.config()

const app = express()
// const port = 3001

// Database connection
mongoose.connect(`mongodb+srv://ninznaag:${process.env.MONGODB_PASSWORD}@cluster0.niv71rw.mongodb.net/e-commerce-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.once('open', () => console.log('Connected to MongoDB!'))


app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes Setup
app.use('/carts', cartRoutes)
app.use('/orders', orderRoutes)
app.use('/products', productRoutes)
app.use('/users', userRoutes)



// Connection status
app.listen(process.env.PORT || 3001, () => {
	console.log(`API is now running on localhost:${process.env.PORT || 3001}`)
})