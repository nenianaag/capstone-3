const Order = require('../models/Order')
const Product = require('../models/Product')

// Non-admin User Checkout (Create Order)
module.exports.createOrder = (data) => {
	if(data.isAdmin){
		let message = Promise.resolve({
			message: 'You are not allowed to process an order.'
		})

		return message.then((value) => {
			return value
		})
	} else {

		let inActiveProducts = 0;

		let promises = data.order.products.map((product) => {
			return Product.findById(product.productId).then((result) => {
				if(result.isActive == false){
					inActiveProducts += 1;
					console.log(inActiveProducts)
				}
			})
		})

		return Promise.all(promises).then((result) => {
			if(inActiveProducts == 0){
				let newOrder = new Order({
					userId: data.userId,
					products: data.order.products,
					totalAmount: data.order.totalAmount
				})

				return newOrder.save().then((newOrder, error) => {
					if(error){
						message = false
					}

					return {message: 'Order successfully created!'}
				})	
			} else {
				let message = Promise.resolve({
					message: "Your order has products that are no longer available."
				})

				return message.then((value) => {
					return value
				})
			}

			
		})
	
	}
}

// Retrieve authenticated user's orders
module.exports.getUserOrders = (data) => {
	if(data.isAdmin) {
		let message = Promise.resolve({
			message: 'You are not allowed to access this.'
		})

		return message.then((value) => {
			return value
		})
	} else {
		return Order.find({userId: data.userId}).then((result) =>{
			if(result == null){
				return {message: "You have no orders."}
			} else {
				return result
			}
		})
	} 
}

// Retrieve all orders (Admin Only)
module.exports.getAllOrders = (data) => {
	if(data.isAdmin){
		return Order.find().then((result) => {
			return result
		})
	} else {
		let message = Promise.resolve({
			message: 'You must be an Admin to retrieve all orders.'
		})

		return message.then((value) => {
			return value
		})
	}	
}
