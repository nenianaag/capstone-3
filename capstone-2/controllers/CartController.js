const Cart = require('../models/Cart')
const Order = require('../models/Order')
const Product = require('../models/Product')

// Add product
module.exports.addCartProduct = (data) => {

	if(data.isAdmin){
		let message = Promise.resolve({
			message: 'You are not allowed to access this.'
		})

		return message.then((value) => {
			return value
		})

	} else {

		return Cart.find({userId: data.userId, productId: data.product.productId}).then((result) => {

			if(result.length === 0){

				let newCart = new Cart({
					userId: data.userId,
					productId: data.product.productId,
					quantity: data.product.quantity
				})

				return newCart.save().then((newCart, error) => {

					if(error){
						return false
					}
					
					return true

				})

			} else {
				return Promise.resolve(false)
			}
		})
	}
}

// Get All Carts of a user
module.exports.getCartByUserId = (data) => {
	console.log(data.userId)
	return Cart.find({userId: data.userId}).then((result) => {
		return result
	})
}

// Get SINGLE Cart
module.exports.getCartByUserIdAndProductId = (data) => {
	return Cart.find({userId: data.userId, productId: data.productId}).then((result) => {
		return result
	})
}

// Change product quantities from cart & subtotal for each item
module.exports.updateCartProduct = (data) => {
	return Cart.find({_id: data.cartId}).then((cartProduct) => {

		if(cartProduct.length === 0){

			return Promise.resolve(false)

		} else {

			return Cart.findByIdAndUpdate(data.cartId, {
				quantity: data.quantity
			}).then((result, error) => {

				if (error) {
					return false
				}

				return true
			})
		}
	})
}

// Remove products from cart
module.exports.deleteCartProduct = (data) => {

	return Cart.findByIdAndDelete(data).then((result) => {
		if(result == null) {
			Promise.resolve(false)
		} else {
			return true;	
		}
	})
}

//Total price for all items (checkout)
module.exports.checkout = (data) => {
	return Cart.find({ userId: data.userId }).then((result) => {

		if (result.length == 0) {
			Promise.resolve(false)
		} else {

			let totalPrice = 0;
			let products = []

			let promises = result.map((cartProduct) => {
				
				products.push({
					productId: cartProduct._id,
					quantity: cartProduct.quantity
				})

				Cart.findByIdAndDelete(cartProduct._id).then((result, error) => {
					if(error){
						return false
					}
				});

				return Product.find({_id: cartProduct.productId}).then((product) =>{
					console.log(cartProduct.productId)
					console.log(cartProduct.quantity)
					console.log(product)
					totalPrice += cartProduct.quantity * product[0].price;
				})
			})

			return Promise.all(promises).then((result) => {
				let newOrder = new Order({
					userId: data.userId,
					products: products,
					totalAmount: totalPrice
				})

				return newOrder.save().then((newOrder, error) => {
					if(error){
						return false
					}

					return true
				})
			})

			
		}
	})
}