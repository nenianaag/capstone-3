// Dependencies
const bcrypt = require('bcrypt')

// Local Imports
const Product = require('../models/Product')
const User = require('../models/User')
const auth = require('../auth')

// User registration with authentication
module.exports.register = (data) => {

	return User.find({ email : data.email}).then((user) => {

		if (user.length > 0) {

			return false;

		} else {

			let encrypted_password = bcrypt.hashSync(data.password, 10)

			let new_user = new User({
				firstName: data.firstName,
				lastName: data.lastName,
				mobileNumber: data.mobileNumber,
				email: data.email,
				password: encrypted_password
			})

			return new_user.save().then((register_user, error) => {
				if(error){
					return false
				}
				return { message: 'User successfully registered!' }
			})
		}
	})	
}

// User Login
module.exports.login = (data) => {
	return User.findOne({email : data.email}).then((result) => {
		if(result == null){
			return {message : "User doesn't exist!"}
		}

		const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

		if(isPasswordCorrect) {
			return {accessToken: auth.createAccessToken(result)}
		}

		return {message: 'Password is incorrect!'}
	})
}

// Toggle admin permission
module.exports.toggleAdmin = (data) => {
	return User.findOne({_id: data.userId}).then((user) => {
		// console.log(user.isAdmin)

		if(data.isAdmin) {
			user.isAdmin = !user.isAdmin;

			return user.save().then((user, error) => {
				if(error){
					return false
				}
				return { message: `${user.firstName} ${user.lastName}'s admin status: ${user.isAdmin}!` }
			})
		} else {
			return {message: 'You must be an administrator to process this request'}
		}
		
	})
}

// Retrieve user details
module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, {password: 0}).then((result) => {
		return result
	})
}


// Get ALL users (added feature)
module.exports.getAllUsers = (data) => {
	if(data.isAdmin) {
		return User.find().then((result) => {
			return result
		}) 
	} else {
		let message = Promise.resolve({
			message: 'User must be an Admin to access this.'
		})

		return message.then((value) => {
			return value
		})
	}	
}
