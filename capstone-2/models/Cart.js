const mongoose = require('mongoose')

const cart_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User Id is required.']
	},
	productId: {
		type: String,
		required: [true, 'Product Id is required.']
	},
	quantity: {
		type: Number,
		required: [true, 'Quantity is required.']
	}
})



module.exports = mongoose.model('Cart', cart_schema)