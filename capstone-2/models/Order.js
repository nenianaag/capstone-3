const mongoose = require('mongoose')

const order_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User Id is required.']
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, 'Product Id is required.']
			},
			quantity: {
				type: Number,
				required: [true, 'Quantity is required.']
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, 'Total amount is required.']
	},
	status: {
		type: String,
		default: "Pending"
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Order', order_schema)