import { useState, useEffect } from 'react'
import { Modal, Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function UpdateProductModal(props){

	// Object deconstruction
	const {show, toggleUpdateModal, productId} = props;

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [productId])

	let updateProduct = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			if(result === true){
				Swal.fire({
					title: 'Great!',
					icon: 'success',
					text: "Your product has been updated successfully"
				})
				toggleUpdateModal()
			} else {
				Swal.fire({
					title: "Ooopps!",
					icon: "error",
					text: "Something went wrong."
				})
			}
		})
	}

	return(
		<>
			<Modal show={show} onHide={toggleUpdateModal}>
				<Modal.Header closeButton>
					<Modal.Title>Update Product</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>

						<Form.Group className="mb-3" controlId="name">
							<Form.Label>Product Name</Form.Label>
							<Form.Control
							type="text"
							placeholder="Enter product name"
							value={name}
							onChange={(e) => setName(e.target.value)}
							autoFocus
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="description">
							<Form.Label>Description</Form.Label>
							<Form.Control 
								as="textarea" 
								rows={3} 
								placeholder="Enter product description"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								autoFocus
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="price">
							<Form.Label>Price</Form.Label>
							<Form.Control
							type="number"
							placeholder="Enter product price"
							value={price}
							onChange={(e) => setPrice(e.target.value)}
							autoFocus
							/>
						</Form.Group>

					</Form>
				</Modal.Body>

				<Modal.Footer>
					<Button variant="secondary" onClick={toggleUpdateModal}>
						Close
					</Button>
					<Button variant="primary" onClick={updateProduct}>
						Save Changes
					</Button>
				</Modal.Footer>
			</Modal>
	    </>
	)
}