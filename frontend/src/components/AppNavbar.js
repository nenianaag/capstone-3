// Base imports
import {useState, useEffect, useContext} from 'react';
import {Link, NavLink} from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

// Local Imports
import UserContext from '../UserContext'


export default function AppNavbar(){

	const {user} = useContext(UserContext)

	console.log(user)

	return(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand as={Link} to="/" className="ms-3">Amazing Bamboo</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto me-3">
					{/*<Nav.Link as={NavLink} to="/">Home</Nav.Link>*/}
					<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
					{ (user.id) ?
						user.isAdmin === true ?
							<>
								<Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
								<Nav.Link as={NavLink} to="/products/create">Add Product</Nav.Link>
								<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
							</>
							:
							<>
								<Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
								<Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
								<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
							</>
						:
						<>
							<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register">Register</Nav.Link>	
						</>
					}							
				</Nav>
			</Navbar.Collapse>
		</Navbar>	
	)
}