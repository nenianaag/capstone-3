import { useState, useEffect, useContext } from 'react'
import { useParams, useNavigate, Link } from 'react-router-dom'

import { Container, Card, Button, Row, Col } from 'react-bootstrap'
import Swal from 'sweetalert2'

import UserContext from '../UserContext'


export default function ProductView() {
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [quantity, setQuantity] = useState(0)
	const [subtotal, setSubtotal] = useState(0)
	const [cartId, setCartId] = useState('')
	const [isProductInCart, setIsProductInCart] = useState(false)

	let subtractQty = () => {
		if(quantity > 0) {
			let total = quantity;
			total -= 1
			setQuantity(total)
			setSubtotal(total * price)
		} 
	}

	let addQty = () => {
		let total = quantity;
		total += 1
		setQuantity(total)
		setSubtotal(total * price)
	}

	let addToCart = () => {

		if(quantity === 0){
			Swal.fire({
				title: 'Invalid Amount!',
				icon: 'error',
				text: 'The quantity cannot be zero!'
			})
		} else {
			fetch(`${process.env.REACT_APP_API_URL}/carts/`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productId: productId,
					quantity: quantity,
					subtotal: subtotal
				})
			})
			.then(response => response.json())
			.then(result => {
				if(result === true){
					Swal.fire({
						title: 'Success!',
						icon: 'success',
						text: 'Product successfully added to cart!'
					})
					setName('')
					setDescription('')
					setPrice('')
					setQuantity(0)
					setSubtotal(0)
					setCartId('')
					setIsProductInCart(false)

					navigate('/products')
				} else {
					Swal.fire({
						title: 'Ooppss!',
						icon: 'error',
						text: 'Something went wrong.'
					})
				}
			})
		}
	}

	let updateCart = () => {
		fetch(`${process.env.REACT_APP_API_URL}/carts/${cartId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				cartId: cartId,
				quantity: quantity
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Cart successfully updated!'
				})
				setName('')
				setDescription('')
				setPrice('')
				setQuantity(0)
				setSubtotal(0)
				setCartId('')
				setIsProductInCart(false)

				navigate('/products')
			} else {
				Swal.fire({
					title: 'Ooppss!',
					icon: 'error',
					text: 'Something went wrong.'
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [productId])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/carts/getCartByUserIdAndProductId`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result.length > 0)
			if(result.length > 0){
				setQuantity(result[0].quantity)
				setSubtotal(result[0].quantity * price)
				setCartId(result[0]._id)
				setIsProductInCart(true)
			}
		})
	}, [productId, price])

	return (
		<>
			<h1 className="text-center my-3">{name}</h1>
			<h3 className="text-center my-3">Description</h3>
			<p className="text-center my-3">{description}</p>			
			<p className="text-center my-3">Price: Php {price}</p>

			{ user.isAdmin === false || !user.id === null ?
				isProductInCart ?
				<>
					<div className="d-flex justify-content-center">
						<div className="qty-control d-flex">
							<Button 
								className="btn btn-warning qty-btn"
								onClick={subtractQty}
							>-</Button>
							
							<input
								className="qty-input text-center"
								type="text"
								value={quantity}
								onChange={(e) => setQuantity(e.target.value)}
							></input>

							<Button 
								className="btn btn-warning qty-btn"
								onClick={addQty}
							>+</Button>
						</div>
					</div>
					<p className="text-center my-3">Subtotal: Php {subtotal}</p>
					<div className="text-center">
						<Button className="btn btn-success" onClick={updateCart}>Update Cart</Button>
					</div>
				</>
				:
				<>
					<div className="d-flex justify-content-center">
						<div className="qty-control d-flex">
							<Button 
								className="btn btn-warning qty-btn"
								onClick={subtractQty}
							>-</Button>
							
							<input
								className="qty-input text-center"
								type="text"
								value={quantity}
								onChange={(e) => setQuantity(e.target.value)}
							></input>

							<Button 
								className="btn btn-warning qty-btn"
								onClick={addQty}
							>+</Button>
						</div>
					</div>
					<p className="text-center my-3">Subtotal: Php {subtotal}</p>
					<div className="text-center">
						<Button className="btn btn-success" onClick={addToCart}>Add To Cart</Button>
					</div>
				</>
			:
				!user.isAdmin === true && user.id === null ?
					<div className="text-center">
						<Link className="btn btn-success" to={`/login`}>Login</Link>
					</div>
				:
					null
			}			
		</>		
	)
}
