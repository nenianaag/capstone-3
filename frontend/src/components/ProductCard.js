import { useState, useEffect, useContext } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import Proptypes from 'prop-types'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'

import UpdateProductModal from './UpdateProductModal'
import UserContext from '../UserContext'

export default function ProductCard({product}){

	const {user, setUser} = useContext(UserContext)

	const {name, description, price, _id, isActive} = product

	const [isAvailable, setIsAvailable] = useState(isActive)
	const [isUpdating, setIsUpdating] = useState(false)
	console.log(localStorage.getItem("isAdmin"))

	let toggleUpdateModal = () => {
		setIsUpdating(!isUpdating)
	}

	let archiveProduct = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result === true) {
				Swal.fire({
					title: "Nice!",
					icon: "success",
					text: "Operation successful!"
				})
			} else {
				Swal.fire({
					title: "Ooopps!",
					icon: "error",
					text: "Operation unsuccessful!"
				})
			}			
		})
	}

	return(
		<Card className="col-lg-4 mx-3 my-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Link className="btn btn-primary mx-1" to={`/products/${_id}`}>Details</Link>

				{ user.isAdmin === true ?
					<Button className="btn btn-warning mx-1" onClick={toggleUpdateModal}>Update</Button>
					:
					null
				}

				{user.isAdmin === true ?
					isAvailable ?
						<Button className="btn btn-danger mx-1" onClick={archiveProduct}>Archive</Button>
							
					:
						<Button className="btn btn-success mx-1" onClick={archiveProduct}>Unarchive</Button>
				:
					null	
				}
			</Card.Body>

			{ isUpdating ?
				<UpdateProductModal show={isUpdating} toggleUpdateModal={toggleUpdateModal} productId={_id}/>
				:
				null
			}
		</Card>
	)


}