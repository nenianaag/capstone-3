import React from 'react'

// Initializes a react context
const UserContext = React.createContext()

// Initializes a context provider
export const UserProvider = UserContext.Provider


export default UserContext