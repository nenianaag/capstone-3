const products_data = [
	{
		id: "bc01",
		name: "Bamboo Charcoal",
		description: "A charcoal made from the species of Bamboo.",
		price: 20,
		isAvailable: true
	},
	{
		id: "bcf02",
		name: "Bamboo Filter",
		description: "A small pouch containing bamboo powder useful for preventing foul odor",
		price: 50,
		isAvailable: true
	},
	{
		id: "bcd03",
		name: "Bamboo Deodorizer",
		description: "An eco-friendly deodorizer that prevents foul odors",
		price: 150,
		isAvailable: true
	},
	{
		id: "bct04",
		name: "Bamboo Charcoal Toothpaste",
		description: "Removes surface stains, and whitens teeth with a refreshingly minty taste",
		price: 200,
		isAvailable: true
	},
	{
		id: "bctwp05",
		name: "Bamboo Charcoal Teeth Whitening Powder",
		description: "It will help to strengthen your gums, remove toxins and bad smell from your mouth, absorb heavy metals and then remineralised by bentonite clay.",
		price: 100,
		isAvailable: true
	},
	{
		id: "bcfc06",
		name: "Bamboo Charcoal Face Soap",
		description: "Natural bamboo extracts unhealthy toxins from beneath your skin to prevent clogged pores, breakouts, and skin irritations.",
		price: 80,
		isAvailable: true
	},
	{
		id: "bcbs07",
		name: "Bamboo Charcoal Body Soap",
		description: "Feel fresh and cleansed with this all-natural, vegan and handmade bamboo charcoal soap that can detoxify your skin and your body.",
		price: 150,
		isAvailable: true
	},
	{
		id: "bcpom08"
		name: "Bamboo Charcoal Peel-off Mask",
		description: "Made with Charcoal and minerals; natural ingredients that help clean dirt and oil from pores allowing moisture to be absorbed into the skin.",
		price: 120,
		isAvailable: true
	}
]

export default products_data