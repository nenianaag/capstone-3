import { useState, useEffect, useContext } from 'react'
import { Table, Button } from 'react-bootstrap';
import UserContext from '../UserContext'

export default function Orders() {

	const {user} = useContext(UserContext);

	const [ordersData, setOrdersData] = useState([]);

	useEffect(() => {
		let fetchUrl = `${process.env.REACT_APP_API_URL}/orders/user-orders`;

		console.log(user.isAdmin === true)
		if (user.isAdmin === true) {
			fetchUrl = `${process.env.REACT_APP_API_URL}/orders`;
		}

		fetch(fetchUrl, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			let data = result.map((order) => {
				return (
					<tr>
						<td>{order._id.substring(10, order._id.length -1)}</td>
						<td>{order.status}</td>
						<td>{order.purchasedOn.substring(0,10)}</td>
						<td>{order.totalAmount}</td>
						<td>
							<Button 
								className="btn btn-primary"
							>Details</Button>
						</td>
					</tr>
				)
			})

			setOrdersData(data);
		})
	}, [])

	return(
		<div className="container">
			<div className="row">
				<h1 className="text-center my-3">Orders Page</h1>
				<Table striped bordered hover className="text-center">
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Status</th>
							<th>Transaction Date</th>
							<th>Total Amount</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{ordersData}
					</tbody>
				</Table>
			</div>
		</div>
	)
}