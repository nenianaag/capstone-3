import { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function AddProduct() {
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	const [isFormComplete, setIsFormComplete] = useState(false)

	function createProduct(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			if(result === true){
				Swal.fire({
					title: "Congratulations!",
					icon: "success",
					text: "Your new product added successfully!"
				})
				setName('')
				setDescription('')
				setPrice('')
			} else {
				Swal.fire({
					title: "Sorry!",
					icon: "error",
					text: "Duplicate product found!"
				})
			}
		})
	}

	useEffect(() => {
		if(name !== '' && description !== '' && price !== '') {
			setIsFormComplete(true)
		} else {
			setIsFormComplete(false)
		}
	},[name, description, price])

	return (
		<div className="container">
			<div className="row">
				<Form className="col-lg-6 offset-lg-3" onSubmit={event => createProduct(event)}>
					<h1 className="my-3 text-center">Add Product</h1>
		            <Form.Group controlId="productName">
		                <Form.Label>Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter product name" 
			                value={name}
			                onChange={event => setName(event.target.value)}
			                required
		                />
		            </Form.Group>

		             <Form.Group controlId="productDescription">
		                <Form.Label>Description</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter product description" 
			                value={description}
			                onChange={event => setDescription(event.target.value)}
			                required
		                />
		            </Form.Group>

		             <Form.Group controlId="price">
		                <Form.Label>Price</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter product price" 
			                value={price}
			                onChange={event => setPrice(event.target.value)}
			                required
		                />
		            </Form.Group>

		            { isFormComplete ?
		            	<Button className="mt-3" variant="success" type="submit">Add Product</Button>
		            :
		            	<Button className="mt-3" variant="danger" type="submit" disabled>Add Product</Button>
		            }
			     </Form>
		     </div>
	     </div>
	)

}
