import ProductCard from '../components/ProductCard'
import { useEffect, useState, useContext } from 'react'
import UserContext from '../UserContext'


export default function Products(){
	const [products, setProducts] = useState([])
	const {user} = useContext(UserContext)

	useEffect(() => {
		let fetchUrl = `${process.env.REACT_APP_API_URL}/products/all-active-products`
		console.log(user.isAdmin)
		if(user.isAdmin === true) {
			
			fetchUrl = `${process.env.REACT_APP_API_URL}/products`
		}

		fetch(fetchUrl, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			const productCards = result.map(product => {
				return(
					<ProductCard key={product._id} product={product}/>
				)
			})
			setProducts(productCards)

		})
	}, [])
	return(
		<>
			<h1 className="text-center my-3">Products Page</h1>
			<div className="container">
				<div className="row justify-content-center">
					{products}
				</div>
			</div>
			
		</>
	)


}