import { useState, useEffect, useContext } from 'react'
import { Table, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'

// Local Imports
import UserContext from '../UserContext'

export default function Cart() {

	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()

	const [cartData, setCartData] = useState({});
	const [cartRows, setCartRows] = useState([]);
	const [total, setTotal] = useState(0);

	let updateCartQty = (e, cartKey) => {
		let data = {...cartData}
		let amount = parseInt(e.target.value)
		if (e.target.value === '') {
			amount = 0
		} else {
			data[cartKey].quantity = amount;
			data[cartKey].subtotal = amount * data[cartKey].price
			console.log(data)
			setCartData(data)
			fetch(`${process.env.REACT_APP_API_URL}/carts/${data[cartKey].cartId}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					cartId: data[cartKey].cartId,
					quantity: data[cartKey].quantity
				})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)
			})
		}
	}

	let subtractQty = (cartKey) => {
		let data = {...cartData}
		if(data[cartKey].quantity > 1) {
			data[cartKey].quantity -= 1
			data[cartKey].subtotal = data[cartKey].quantity * data[cartKey].price
			setCartData(data)
			fetch(`${process.env.REACT_APP_API_URL}/carts/${data[cartKey].cartId}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					cartId: data[cartKey].cartId,
					quantity: data[cartKey].quantity
				})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)
			})
		}
	}

	let addQty = (cartKey) => {
		let data = {...cartData}
		data[cartKey].quantity += 1
		data[cartKey].subtotal = data[cartKey].quantity * data[cartKey].price
		setCartData(data)
		fetch(`${process.env.REACT_APP_API_URL}/carts/${data[cartKey].cartId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				cartId: data[cartKey].cartId,
				quantity: data[cartKey].quantity
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
		})
	}

	let checkout = () => {
		let data = {...cartData}
		fetch(`${process.env.REACT_APP_API_URL}/carts/checkout`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			if (result === true) {
				Swal.fire({
					title: "Checkout successful!",
					icon: 'success',
					text: "The products have been successfully checked out. Thank you for shopping!"
				})
				navigate('/products')
			} else {
				Swal.fire({
					title: "Ooppss!",
					icon: 'error',
					text: "Something went wrong"
				})
			}
		})
	}

	let deleteCartProduct = (cartKey) => {
		let data = {...cartData}
		console.log(data[cartKey].cartId)
		fetch(`${process.env.REACT_APP_API_URL}/carts/${data[cartKey].cartId}`, {
			method: 'Delete',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			if (result === true) {
				Swal.fire({
					title: "Cart updated!",
					icon: 'success',
					text: "Cart product successfully removed!"
				})
				delete data[cartKey];
				setCartData(data);
			} else {
				Swal.fire({
					title: "Ooppss!",
					icon: 'error',
					text: "Something went wrong"
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/carts/getCartByUserId`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			let completeData = result.map((cart) => {
				return fetch(`${process.env.REACT_APP_API_URL}/products/${cart.productId}`, {
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(response => response.json())
				.then(result => {
					console.log(result)

					return {
						cartId: cart._id,
						name: result.name,
						description: result.description,
						price: result.price,
						quantity: cart.quantity,
						subtotal: cart.quantity * result.price
					}
				})
			})

			Promise.all(completeData)
			.then(result => {
				console.log(result)

				let finalData = {}

				result.forEach((cart) => {
					finalData[cart.name] = {
						cartId: cart.cartId,
						description: cart.description,
						price: cart.price,
						quantity: cart.quantity,
						subtotal: cart.subtotal
					}
				})

				console.log(finalData)
				setCartData(finalData)
			})

		})
	}, [])

	useEffect(() => {
		console.log(cartData);

		let totalAmount = 0;

		let cartKeys = Object.keys(cartData);

		let cartElements = cartKeys.map((cartKey) => {

			totalAmount += cartData[cartKey].subtotal;

			return (
				<tr>
					<td>{cartKey}</td>
					<td>{cartData[cartKey].description}</td>
					<td>{cartData[cartKey].price}</td>
					<td>
						<div className="d-flex justify-content-center">
							<div className="qty-control d-flex">
								<Button 
									className="btn btn-warning qty-btn"
									onClick={() => subtractQty(cartKey)}
								>-</Button>
								
								<input
									className="qty-input text-center"
									type="text"
									value={cartData[cartKey].quantity}
									onChange={(e) => updateCartQty(e, cartKey)}
								></input>

								<Button 
									className="btn btn-warning qty-btn"
									onClick={() => addQty(cartKey)}
								>+</Button>
							</div>
						</div>
					</td>
					<td>{cartData[cartKey].subtotal}</td>
					<td>
						<Button 
							className="btn btn-danger"
							onClick={() => deleteCartProduct(cartKey)}
						>Delete</Button>
					</td>
				</tr>
			)
		})

		setTotal(totalAmount);
		setCartRows(cartElements);
	}, [cartData])

	return (
		<div className="container">
			<div className="row">
				<h1 className="text-center my-3">Cart Page</h1>
				<Table striped bordered hover className="text-center">
					<thead>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{ cartRows.length > 0 ?
							cartRows
							:
							<tr>
								<td colspan="6">
									Cart is Empty
								</td>
							</tr>
						}
						<tr>
							<td className="text-end fw-bold mr-3" colspan="4">Total</td>
							<td>{total}</td>
							<td></td>
						</tr>
					</tbody>
				</Table>
				<div className="text-center">
					<Button 
						className="btn btn-success"
						onClick={checkout}
					>
						Checkout
					</Button>
				</div>
			</div>
		</div>
	)
}