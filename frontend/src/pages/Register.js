// Base Imports
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';

// Dependencies
import { Form, Button, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';

// Local Imports
import UserContext from '../UserContext';

export default function Register(){

	const {user} = useContext(UserContext)

	// Initialize navigation
	const navigate = useNavigate()


	const [firstName, setFirstName] = useState('') 
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	// For button enable and disable
	const [isActive, setIsActive] = useState(false)


	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNumber: mobileNumber,
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === false){
				Swal.fire({
					title: 'Whoops!',
					icon: 'error',
					text: 'Something went wrong! Kindy check further your provided details. Also try using a different email.'
				})
			} else {
				// To clear out the input fields after form submission
				setFirstName('')
				setLastName('')
				setMobileNumber('')
				setEmail('')
				setPassword1('')
				setPassword2('')

				Swal.fire({
					title: "Hooray! Welcome to our Family!",
					icon: "success",
					text: "You've register successfully!"
				})

				navigate("/login")
			}
		})

	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2])


	return(
		(user.id !== null) ?
			<Navigate to="/login"/>
		:
		<div className="container">
			<div className="row">
		        <Card className="p-3 col-lg-6 offset-lg-3 my-5">
					<Form className="justify-content-center" onSubmit={event => registerUser(event)}>
						<h1 className="text-center">Register</h1>
						<div className="d-flex justify-content-center">
				            <Form.Group className="mx-3 my-1" controlId="firstName">
				                <Form.Label>First Name</Form.Label>
				                <Form.Control 
					                type="text" 
					                placeholder="Enter your first name" 
					                value={firstName}
					                onChange={event => setFirstName(event.target.value)}
					                required
				                />
				            </Form.Group>

			                <Form.Group className="mx-3 my-1" controlId="lastName">
			                    <Form.Label>Last Name</Form.Label>
			                    <Form.Control 
			    	                type="text" 
			    	                placeholder="Enter your last name" 
			    	                value={lastName}
			    	                onChange={event => setLastName(event.target.value)}
			    	                required
			                    />
			                </Form.Group>
		                </div>

		                <div className="d-flex justify-content-center">
			                <Form.Group className="mx-3 my-1" controlId="mobileNumber">
			                    <Form.Label>Mobile Number</Form.Label>
			                    <Form.Control 
			    	                type="text" 
			    	                placeholder="Enter a mobile number" 
			    	                value={mobileNumber}
			    	                onChange={event => setMobileNumber(event.target.value)}
			    	                required
			                    />
			                </Form.Group>

				            <Form.Group className="mx-3 my-1" controlId="userEmail">
				                <Form.Label>Email Address</Form.Label>
				                <Form.Control 
					                type="email" 
					                placeholder="Enter your email" 
					                value={email}
					                onChange={event => setEmail(event.target.value)}
					                required
				                />
				            </Form.Group>
				        </div>

			            <div className="d-flex justify-content-center">
				            <Form.Group className="mx-3 my-1" controlId="password1">
				                <Form.Label>Password</Form.Label>
				                <Form.Control 
					                type="password" 
					                placeholder="Preferred Password" 
					                value={password1}
					                onChange={event => setPassword1(event.target.value)}
					                required
				                />
				            </Form.Group>

				            <Form.Group className="mx-3 my-1" controlId="password2">
				                <Form.Label>Verify Password</Form.Label>
				                <Form.Control 
					                type="password" 
					                placeholder="Verify Password" 
					                value={password2}
					                onChange={event => setPassword2(event.target.value)}
					                required
				                />
				            </Form.Group>
				        </div>
				        <p className="text-muted text-center">
				            We'll never share your details with anyone else.
				        </p>

				        <div className="text-center">
			            {	isActive ? 
			            	<Button className="my-2" variant="primary" type="submit" id="submitBtn">
			            		Submit
			            	</Button>
			            	: 
			            	<Button className="my-2" variant="primary" type="submit" id="submitBtn" disabled>
			            		Submit
			            	</Button>
			            }
			            </div>          
			        </Form>
			    </Card>
			</div>
	    </div>
	)

}

	