// Base Imports
import './App.css';
import { useState } from 'react';

// Dependencies
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// Local Imports
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';

import AddProduct from './pages/AddProduct';
import Cart from './pages/Cart';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Orders from './pages/Orders';
import Products from './pages/Products';
import Register from './pages/Register';


import { UserProvider } from './UserContext';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
      <>
         {/*Provides the user context throughout any component inside of it.*/}
          <UserProvider value={{user, setUser, unsetUser}}>
            {/*Initializes that dynamic routing will be involved*/}
              <Router>
                  <AppNavbar/>
                  <Container>       
                      <Routes>
                     {/* <Route path="/" element={<Home/>}/>
                          <Route path="/products" element={<Products/>}/>
                          }/>*/}
                          <Route path="/products/create" element={<AddProduct/>}/>
                          <Route path="/cart" element={<Cart/>}/>
                          <Route path="/products/:productId" element={<ProductView/>}/>
                          <Route path="/login" element={<Login/>}/>
                          <Route path="/logout" element={<Logout/>}/>
                          <Route path="/orders" element={<Orders/>}/>
                          <Route path="/products" element={<Products/>}/>
                          <Route path="/register" element={<Register/>}/>
                   {/*       <Route path="*" element={<ErrorPage/>}/>*/}
                      </Routes>
                  </Container>
                </Router>         
          </UserProvider>   
      </>
    );
}

export default App;
